import gym
import tensorflow as tf
import numpy as np
import pandas as pd


discount=.9
learningrate=.001
epsilon=.005 # Random action selection chance

env=gym.make('CartPole-v0')
# env.step(action):
# 1. Observation
# 2. Reward
# 3. Done
# 4. Info
state_space_shape=env.observation_space.shape[0]
action_space_shape=env.action_space.n # Assumes discrete right now


# Will hold elements in the form of [ [space i]]
# Maybe not necessary for cart-and-pole example
memory=[]

net=tf.keras.Sequential([
    tf.keras.Dense(5,input_shape=env.observation_space.shape,activation=tf.nn.linear),
    tf.keras.Dense(5,activation='linear'),
    tf.keras.Dense(env.action_space.n)]
    )

net.compile(optimizer='adam',loss='quadratic')

for _ in np.arange(100000): # 100k episodes!
    if np.mod(_,1000)==0:
        print('On episode number {}'.format(_))
        
    done=False
    state=env.reset()
    
    while !done:
        if np.random.uniform()<epsilon:
            action=env.action_space.sample()
            next_state=env.step(action)
            #memory.append([state,action,next_state[0]])
        else:
            output=net.evaluate(state)
            next_state=env.step(output.argmax())
            #memory.append([state,output.argmax(),next_state[0]])

        if np.mod(_,1000)==0:
            env.render()
        
        done, reward =next_state[2], next_state[1]

        target = np.zeros(action_space_shape)
        target[output.argmax()] = reward + discount*next_state[0].max()

        out=np.zeros(action_space_shape)
        out[output.argmax()] = output.max()
        
        
def dumb_net(space):

    if space.shape[0]==env.observation_space.shape[0]: # If only passing a space then I just need a feed forward
       dense1=tf.layers.dense(
            inputs=space,
            units=5,
            activation=tf.nn.linear
            )

        dense2=tf.layers.dense(
            inputs=dense1,
            units=5,
            activation=tf.nn.linear
            )

        output=tf.layers.dense( # Default logistic activation
            inputs=dense3,
            units=2
            )

        return output
    else:
        global state
        dense1=tf.layers.dense(
            inputs=space,
            units=5,
            activation=tf.nn.linear
            )

        dense2=tf.layers.dense(
            inputs=dense1,
            units=5,
            activation=tf.nn.linear
            )

        output1=tf.layers.dense( # Default logistic activation
            inputs=dense3,
            units=2
            )
        
    
