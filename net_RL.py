import numpy as np
import gym
import random
#import matplotlib.pyplot as plt
import seaborn as sns

# From SHL_scratch:
class nnet:
    def __init__(self,inputs,hidden_layer,output_count):
        # Initialize using random normal

        # Assign weights and biases from input to first layer - initialize then build out
        # Unlike SHL_scratch doesn't expect the 'inputs' argument to be a data.frame. Just value
        weights=[np.random.normal(scale=.1,size=(inputs,hidden_layer[0]))]
        biases=[np.random.normal(scale=.1,size=(1,hidden_layer[0]))]
        #weights=np.zeros(shape=(inputs.shape[0],hidden_layer[0]))
        #biases=np.zeros(shape=(1,hidden_layer[0]))
        number_hidden=len(hidden_layer)

        # These could probably be handled better
        if number_hidden>1:
            for i in list(range(1,number_hidden+1)):
                if (i+1)>number_hidden:
                    weights.append(np.random.normal(scale=.1,size=(weights[i-1].shape[1],output_count)))
                    biases.append(np.random.normal(scale=.1,size=(1,output_count)))
                    #weights=[weights,np.zeros(shape=(weights[i-1].shape[1],output_count))]
                    #biases=[biases,np.zeros(shape=(1,output_count))]
                else:
                    weights.append(np.random.normal(scale=.1,size=(weights[i-1].shape[1],hidden_layer[i])))
                    biases.append(np.random.normal(scale=.1,size=(1,hidden_layer[i])))
                    #weights=[weights,np.zeros(shape=(weights[i-1].shape[1],hidden_layer[i]))]
                    #biases=[biases,np.zeros(shape=(1,hidden_layer[i]))]
        else:
            weights.append(np.random.normal(scale=.1,size=(weights[0].shape[1],output_count)))
            biases.append(np.random.normal(scale=.1,size=(1,output_count)))
            #weights=[weights,np.zeros(shape=(weights.shape[1],output_count))]
            #biases=[biases,np.zeros(shape=(1,output_count))]

        self.weights=weights
        self.biases=biases
        self.activations=[]

    def feedforward(self,inputs):
        prevlayer_train=[inputs]
        for j in list(range(0,len(self.weights))):
            predictions_train=activ['activ'](prevlayer_train[len(prevlayer_train)-1],self.weights[j])#,RLnet.biases[j])
            prevlayer_train.append(predictions_train)

        self.activations=prevlayer_train # Store the most recent activations
        return predictions_train

    def backprop(self,p,t): # Always uses currently stores activations
        dl=loss['deriv'](p,t)*activ['deriv'](p)               
        #print(RLnet.weights[len(RLnet.weights)-1])

        ### Backpropogate the error from the last action used to step
        for k in np.arange(len(self.weights)-1,-1,-1):
            #self.biases[k]=RLnet.biases[k]-learningrate*dl.mean(axis=0) # Change the biases in this layer. As best I can tell this mean is correct - dl is what's supposed to be calculated

            # Calculate gradient - average across minibatch gradient
            dCdW=dC(dl,self.activations[k])

            # Reset dl before changing weights - depends on each sample image
            dl = np.matmul(dl,self.weights[k].transpose())*activ['deriv'](self.activations[k])

            # Adjust the weights
            self.weights[k]=self.weights[k]-learningrate*dCdW - lmda*self.weights[k]
            #if k==(len(RLnet.weights)-1):
                #print(learningrate*dCdW[0,0].round(4),lmda*self.weights[len(RLnet.weights)-1][0,0].round(4))
                
def activation(name):
    # Has an activation and a derivative of activation with respect to z = Weights*Layer Inputs + bias
    return {
        'relu':{'activ':lambda A,B: np.maximum(np.matmul(A,B),0),#-np.repeat(b,A.shape[0],axis=0),0),
                'deriv':lambda b: 1*(b>0) },#RELU activation. Derivative is 1 only if activation>0
        'sigmoid':{'activ':lambda A,B: 1.0/(1.0+np.exp(-1*(np.matmul(A,B)))),#-np.repeat(b,A.shape[0],axis=0)))),
                   'deriv': lambda b: b*(1-b)},#LOGIT. Derivative trick: activation * (1-activation) = derivative
        'linear':{'activ':lambda A,B: np.matmul(A,B),#-np.repeat(b,A.shape[0],axis=0),
                  'deriv': lambda b: 1 }
    }[name]
    

def normfunction(weights):
    # Input the array of weight matrices - output loss from normalization (quadratic)
    normloss=0
    for w in weights:
        normloss=normloss+w.dot(w.transpose()).sum()

    return normloss

def lossfun(name):
    # Has a loss calculation and a derivative of loss with respect to activation
    return { # Cross entropy is not well defined for activations not in [0,1]
        'crossentropy':{'loss': lambda prediction, actual: -1*np.sum((actual*np.log(prediction,where=prediction>0))+((1-actual)*np.log(1-prediction,where=prediction!=1))) + normfunction(RLnet1.weights),# Calculated a single value
                        'deriv': lambda prediction, actual: np.divide((actual-prediction),(prediction*(1-prediction)),where=actual>0)},#Calculate a 1D array for each sample
        'quadratic':{'loss': lambda prediction, actual: .5*np.sum((prediction-actual)**2)/len(prediction) + normfunction(RLnet1.weights),
                     'deriv': lambda prediction, actual: prediction-actual}
    }[name]

def dC(error_l,layer_lminus1):
    # Given two lists of layer l and l-1 outputs for each image in the minibatch, what is the gradient matrix?
    # for weight i,k it should be the average of activation i in layer l-1 times the error k in layer l
    return np.matmul(layer_lminus1.transpose(),error_l)/layer_lminus1.shape[0]

env=gym.make('CartPole-v0')

state_space_shape=env.observation_space.shape[0]
action_space_shape=env.action_space.n
max_memory_size= 100000 #Max len(memory); number of transitions stored
discount=.9
starting_epsilon=.7
final_epsilon=.005 # Random action selection chance

n_hidden='[8,8]'
activationfn='linear' 
lossfn='quadratic'
n_previous_frames=3 # For cart-pole this is realistically <8 (seeding first with random actions lasts at least 8 frames)
num_episodes=2000
n_generations=200 # Number of (observation building) -> (training) steps for the two networks

show_episodes_every=100
n_minibatch=4
minibatch_size= 2000 #Number of frames from all episodes sampled

learningrate, lmda = -.015,.000000
# Activation function
activ=activation(activationfn)

# Loss function
loss=lossfun(lossfn)

# Assign # of neurons for given layers
hidden_layer_neurons=eval(n_hidden)

#make the nets: One to choose the actions (1), the other to act as target (2)
RLnet1=nnet(state_space_shape*n_previous_frames,hidden_layer_neurons,action_space_shape)
RLnet2=nnet(state_space_shape*n_previous_frames,hidden_layer_neurons,action_space_shape)

#Initialize memory
memory=[]

# Keep all of the rewards of all generations
total_rewards=[]
    
for generation in np.arange(n_generations):
    
    # Show what the agent can do at the beginning of every generation
##    done=False
##    total_reward=0
##    prevlayer_train=[env.reset().reshape((1,state_space_shape))]
##    prev_frames=np.zeros((n_previous_frames,state_space_shape))
##
##    prev_frames[:,:]=prevlayer_train[0]
    
    #Get the first n_previous_frames of stepping before beginnin
    #for frame in np.arange(1,n_previous_frames):
        #action=env.action_space.sample()
        #state=env.step(action)
        #next_frame,done,total_reward=state[0].reshape((1,state_space_shape)),state[2],total_reward+state[1]
        #prev_frames[frame,:]=next_frame
        #env.render()

    # At the beginning of each generation it will show how it performs in an example episode
##    while not done:
##        action=RLnet1.feedforward(prev_frames.reshape(1,state_space_shape*n_previous_frames)).argmax()
##        state=env.step(action)
##        env.render()
##
##        prev_frames[0:n_previous_frames-1,:]=prev_frames[1:n_previous_frames,:]
##        prev_frames[n_previous_frames-1,:]=state[0]
##        total_reward+=state[1]
##        done=state[2]


    ### Generate memories for the training
    for episode in np.arange(num_episodes): # Number of episodes per generation
        #if np.mod(episode,show_episodes_every)==0:
            #env.render()
        total_reward=0
        
        # Get the first observation 
        prevlayer_train=[env.reset().reshape((1,state_space_shape))]
        prev_frames=np.zeros((n_previous_frames,state_space_shape))

        # Put into matrix shape
        prev_frames[:,:]=prevlayer_train[0]
        
        #Get the first n_previous_frames of stepping before beginning the feed forward -> backprop part
        #for frame in np.arange(1,n_previous_frames):
            #action=env.action_space.sample()
            #state=env.step(action)
            #next_frame,done,total_reward=state[0].reshape((1,state_space_shape)),state[2],total_reward+state[1]
            #prev_frames[frame,:]=next_frame
            #action=env.action_space.sample()

        reward=1
        predictions_train = RLnet1.feedforward(prev_frames.reshape(1,state_space_shape*n_previous_frames))
        
        if np.random.uniform()<(starting_epsilon-(starting_epsilon-final_epsilon)*generation/n_generations): # Linear fall-off of random chance starting at 50% chance
            action=env.action_space.sample()
        else:
            action = predictions_train.argmax() # Which action it is

        done=False
        
        while not done:
            
            #if np.mod(episode,show_episodes_every)==0:
                #env.render()
            state=env.step(action)
            
            # Add this to memory! Will reshape new state and append to this observation to complete (s,a,r,s')
            memory.append([prev_frames.reshape(1,state_space_shape*n_previous_frames),action,reward,done])
            
            prev_frames[0:n_previous_frames-1,:]=prev_frames[1:n_previous_frames,:]
            prev_frames[n_previous_frames-1,:]=state[0]

            # Add this new state to the latest memory entry
            memory[len(memory)-1].append(prev_frames.reshape(1,state_space_shape*n_previous_frames))
            
            reward, done =state[1], state[2] 
            total_reward+=reward

            predictions_train = RLnet1.feedforward(prev_frames.reshape(1,state_space_shape*n_previous_frames))
            
            ### Now that we backpropogated the last action, we just pick the new one and
            if np.random.uniform()<(starting_epsilon-(starting_epsilon-final_epsilon)*generation/n_generations):
                action=env.action_space.sample()
            else:
                action = predictions_train.argmax() # Which action it is

        total_rewards.append(total_reward)
        memory=memory[-max_memory_size:]

    if np.mod(generation,round(n_generations/100))==0:
        plot=sns.distplot(total_rewards[-num_episodes:],kde=False,bins=np.arange(200)+1) # Plot of 10000 random-agent rewards
        plot.set_title('Generation {} Rewards'.format(generation))
        plot.set_xlabel('Total Episode Reward')
        plot.set_ylabel('Count')
        plot.set(ylim=(0,50))

        plot=plot.get_figure()
        plot.savefig('/Users/magikarp/Library/Mobile Documents/com~apple~CloudDocs/RL personal course/CartPole benchmarking/generation_{}_of_{}'.format(generation,n_generations))
        plot.clf()
        
    print('Generation {} test score avg: {} ± {}'.format(generation,np.mean(total_rewards[-num_episodes:]).round(1),np.std(total_rewards[-num_episodes:]).round(2)))    

#    plt.scatter(np.arange(len(total_rewards)),total_rewards,cmap=np.repeat([g for g in list(range(generation))],(generation+1)*num_episodes),s=2)
#    plt.show()
    ### End of getting the memories
    ### Beginning of the backpropogation part

    
    for batch in np.arange(n_minibatch):
        # Create a set where indices (row indices in case of matrices) correspond to transition memories
        memories=np.random.choice(np.arange(np.shape(memory)[0]),minibatch_size,p=np.repeat(np.square(total_rewards),total_rewards)[-max_memory_size:]/np.sum(np.repeat(np.square(total_rewards),total_rewards)[-max_memory_size:]))
        #memories=memory[memories]
        
        states=np.matrix(np.reshape([memory[k][0] for k in memories],(minibatch_size,state_space_shape*n_previous_frames)))
        rewards=np.array([memory[k][2] for k in memories])
        actions=np.array([memory[k][1] for k in memories])
        next_states=np.matrix(np.reshape([memory[k][4] for k in memories],(minibatch_size,state_space_shape*n_previous_frames)))
        dones=np.array([memory[k][3] for k in memories])

        # Preds and target for network 1
        preds1=np.zeros((minibatch_size,action_space_shape))
        target1=np.zeros((minibatch_size,action_space_shape))

        
        output1=RLnet1.feedforward(states) # Prediction
        next_output1=RLnet2.feedforward(next_states) # Contains the values of target
        nextstate_actions1=RLnet1.feedforward(next_states).argmax(axis=1) # Determines which action to train on in the next state evaluation

        # Preds and target for network 2
        preds2=np.zeros((minibatch_size,action_space_shape))
        target2=np.zeros((minibatch_size,action_space_shape))

        output2=RLnet2.feedforward(states)
        next_output2=RLnet1.feedforward(next_states)
        nextstate_actions2=RLnet2.feedforward(next_states).argmax(axis=1)
        

        # This could be improved
        for k in np.arange(minibatch_size):
            preds1[k,actions[k]]=output1[k][0,actions[k]]
            target1[k,actions[k]]=rewards[k]+discount*next_output1[k][0,nextstate_actions1[k,0]] if not dones[k] else rewards[k]

            preds2[k,actions[k]]=output2[k][0,actions[k]]
            target2[k,actions[k]]=rewards[k]+discount*next_output2[k][0,nextstate_actions2[k,0]] if not dones[k] else rewards[k]

        print('Loss: {} ({} from norm)'.format(loss['loss'](preds1,target1).round(4),normfunction(RLnet1.weights).round(4)))
        
        RLnet1.backprop(preds1,target1)
        RLnet2.backprop(preds2,target2)
                   
